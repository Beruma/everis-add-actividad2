package dam2.add.p2;

//Creo la clase profesor que hereda de persona
public class Profesor extends Persona{

	//Creo la variable que solo tendra esta clase
	private String materia;

	//Creo los getters y los setters
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}

	//Creo el toString para poder imprimir por pantalla los datos del arraylist
    @Override
    public String toString() {
        return "PROFESOR " + super.toString() +
                "materia : " + materia;
    }
}
