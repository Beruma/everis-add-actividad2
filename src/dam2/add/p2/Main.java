package dam2.add.p2;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	//Creo un menu dentro del main donde le doy al usuario varias opciones
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		//LLamo primero el m�todo arrancar antes de que se inicie el men�
		Operaciones.arrancar();

		Scanner key = new Scanner(System.in);
		boolean salir = false;

		while (!salir) {
			System.out.println("Elige una opci�n: ");
			System.out.println("1. A�adir persona ");
			System.out.println("2. Felicitar ");
			System.out.println("3. Listar Personas ");
			System.out.println("4. Grabar Personas ");
			System.out.println("5. Salir ");
			int opcion = key.nextInt();

			switch (opcion) {
				case 1:
					Operaciones.anadirPersona();
					break;
				case 2:
					Operaciones.felicitar();
					break;
				case 3:
					Operaciones.listarPersonas();
					break;
				case 4:
					Operaciones.grabarPersonas();
					break;
				case 5:
					Operaciones.grabarPersonas();
					salir = true;
					break;
				default:
					System.out.println("Opci�n no v�lida por favor elija un n�mero del 1 al 5");
			}
		}
	}
}