package dam2.add.p2;

import sun.awt.geom.AreaOp;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Operaciones {
	//He creado estas dos variables globales ya que las uso en m�s de un m�todo//
	private static ArrayList<Persona> ACADEMIA = new ArrayList<>();
	private static Scanner KEY = new Scanner(System.in);

	//Creo un m�todo para que me cree el archivo personas.dat o si existe para que lo lea y almacene los datos en el ArrayList ACADEMIA
	public static void arrancar() {
		File f = new File("Recursos/Personas.dat");

		if (f.exists()) {
			while (f != null) {
				try {
					FileInputStream fi = new FileInputStream("Recursos/Personas.dat");
					ObjectInputStream recuperarPersonas = new ObjectInputStream(fi);
					Persona p = (Persona) recuperarPersonas.readObject();
					ACADEMIA.add(p);
				} catch (ClassNotFoundException i) {
					System.out.println("No existe el archivo");
				} catch (IOException e) {
					System.out.println("Fin del archivo o archivo vacio");
				}
			}
		}
	}
    //Agrego los datos de los usuarios en los objetos alumno o profesor dependiendo de lo que escoja el usuario y luego los a�ado al ArrayList ACADEMIA
	public static void anadirPersona() {

		boolean salir = false;
		//Creo un bucle por si el usuario quiere registrar a m�s de una persona
		do {
			//Pido los datos comunes
			System.out.println("�Eres alumno o profesor?(1-alumno/2-profesor)");
			int opcion = KEY.nextInt();
			KEY.nextLine();

			System.out.println("�Introduzca su nombre y apellidos?");
			String nombre = KEY.nextLine();

			System.out.println("Introduzca la fecha de su cumplea�os (primero el dia y despu�s presiona enter e introduce el mes)");
			int dia = KEY.nextInt();
			int mes = KEY.nextInt();

			//Compruebo que la fecha es correcta
			Calendar calendar = Calendar.getInstance();
			calendar.setLenient(false);
			calendar.set(Calendar.MONTH, mes - 1);
			calendar.set(Calendar.DAY_OF_MONTH, dia);
			Date cumple = calendar.getTime();
			SimpleDateFormat fCumple = new SimpleDateFormat("dd/MM");
			String fecha = fCumple.format(cumple);
            System.out.println("�Cu�l es tu grupo?");
            String grupo = KEY.nextLine();

			//Creo una ramificaci�n dependiendo de si el usuario es alumno o profesor y los agrego al Arraylist ACADEMIA
			switch (opcion) {
				case 1:

					int numeroMatricula = (int) (Math.random() * 101);
					String matricula = "M" + numeroMatricula;
					KEY.nextLine();
					Alumno alumno = new Alumno();
					alumno.setNombre(nombre);
					alumno.setFechaCumple(fecha);
					alumno.setMatricula(matricula);
					alumno.setGrupo(grupo);
					ACADEMIA.add(alumno);
					break;
				case 2:
					KEY.nextLine();
					System.out.println("�Qu� materia impartes?");
					String materia = KEY.nextLine();
					Profesor profesor = new Profesor();
					profesor.setNombre(nombre);
					profesor.setFechaCumple(fecha);
					profesor.setMateria(materia);
					profesor.setGrupo(grupo);
					ACADEMIA.add(profesor);
					break;
				default:
					System.out.println("Opci�n no v�lida");
			}

			System.out.println("�Desea introducir otra persona?(1-si/ 2-no)");
			int eleccion = KEY.nextInt();

			if (eleccion == 2) {
				salir = true;
			} else {
				salir = false;
			}

		} while (!salir);

	}

	//Metodo para saber las fechas de cumplea�os
	public static void felicitar() {

		//Le pido la fecha que quiere saber el usuario
		System.out.println("Introduce la fecha para saber qui�n cumple los a�os");
		int dia = KEY.nextInt();
		int mes = KEY.nextInt();

		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);
		calendar.set(Calendar.MONTH, mes - 1);
		calendar.set(Calendar.DAY_OF_MONTH, dia);
		Date cumple = calendar.getTime();
		SimpleDateFormat fCumple = new SimpleDateFormat("dd/MM");
		String fecha = fCumple.format(cumple);

//		//Recorro el ArrayList en busca de coincidencias de fecha.
		for (Persona i : ACADEMIA) {
			if (i.getFechaCumple().equals(fecha)) {
				if (i instanceof Alumno) {
					System.out.println("FELICIDADES: ALUMNO: " + i.getNombre() + " " + i.getFechaCumple());
				}else if(i instanceof Profesor){
					System.out.println("FELICIDADES: PROFESOR: " + i.getNombre() + " " + i.getFechaCumple());
				}
			}
		}
	}
	//Muestro las personas que aparecen en el archivo Personas.dat
	public static void listarPersonas() {

			//Recorro el array mostrando los datos
			for (int i = 0; i <ACADEMIA.size(); i++) {
				System.out.println(ACADEMIA.get(i));
			}
	}

	//Graba los datos en el archivo Personas.dat
	public static void grabarPersonas() {

		//Escribo en el archivo los objetos persona que se encuentran en el arrayList con los datos de los usuarios
		try {
			ObjectOutputStream escribirPersonas = new ObjectOutputStream(new FileOutputStream("Recursos/Personas.dat", true));
            for (Persona p : ACADEMIA) {
                escribirPersonas.writeObject(p);
            }
			escribirPersonas.close();
		} catch (IOException e) {
			System.out.println("No se ha podido crear el archivo o grabar los datos en �l");
		}
	}
}