package dam2.add.p2;

//Creo la clase Alumno que hereda de la clase Persona
public class Alumno extends Persona{

	//Inicializo la variable que es solo de su clase
	private String matricula;

	//Creo los getters y los setters
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	//Creo el metodo toString para poder imprimir por pantalla los datos del arraylist
    @Override
    public String toString() {
        return "ALUMNO " + super.toString() +
                "matricula : " + matricula;
    }
}
