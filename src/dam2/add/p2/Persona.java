package dam2.add.p2;

import java.io.Serializable;
//Creo la clase persona que sera la responsable de la serializacion del archivo, ya que es la clase padre

public class Persona implements Serializable{

	private String nombre;
	private String fechaCumple;

//Creo los getters y setters

	public String getNombre() {

		return nombre;
	}
	public void setNombre(String nombre) {

		this.nombre = nombre;
	}
	public String getFechaCumple() {

		return fechaCumple;
	}
	public void setFechaCumple(String fechaCumple) {

		this.fechaCumple = fechaCumple;
	}
	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	private String grupo;
	//Creo el toString para poder imprimir porpantalla los ArrayList de objetos persona
	@Override
	public String toString() {
		return "nombre : " + nombre +
				", fecha de Cumpleaños : " + fechaCumple + ", grupo : " + grupo;
	}
}
