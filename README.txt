Lo primero que cree fue el menú de inicio en la clase Main.
Después cree las clases Persona (clase padre), Alumno y Profesor (clases hijas) todas con sus getters y setters.
A la clase Persona le implemente el serializable haciendo que asi también lo heredaran las clases hijas.
Luego cree una clase Operaciones para meter todos los métodos que se llamarían desde el menú o antes de iniciar el menú.
El primero método en iniciar es arrancar(), que me comprueba si existe el fichero Personas.dat y si existe almacena los
objetos Persona en un ArrayList, para así manejar los datos en los distintos métodos que lo necesiten.
El método anadirPersona(), le pide los datos a los usuarios y crea los objetos Alumno o Profesor según corresponda y los
almacena en el ArrayList ACADEMIA que es el arrayList mencionado anteriormente. Si este ArrayList ya contiene datos no
los borra ni se sobreescribe.
En el método felicitar(), lo que hago es que recorro el ArrayList en busca de la coincidencia de la fecha que el usuario
haya introducido. Separo con instanceof entre Alumno y profesor para mostrar si es Alumno o Profesor y llamo al getNombre
y al getFechaCumple para que muestre solo el nombre y la fecha.
En el método listarPersonas(), recorro el ArrayLlist y muestro lo que hay en él.
En el método grabar introduzco los datos que hay en el ArrayList en el archivo Personas.dat, este método se puede hacer
pulsando el 4 en el menú, pero también se le llama al pulsar 5 (salir) en el menú para que no se pierdan los datos
introducidos.
